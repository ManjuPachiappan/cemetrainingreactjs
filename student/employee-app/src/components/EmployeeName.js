import React from "react";

const EmployeeName = (props) => <h6 className="card-text">{props.name}</h6>;

export default EmployeeName;
