import React from "react";

const EmployeeEmail = (props) => props.visible && <h6>{props.email}</h6>;

export default EmployeeEmail;
