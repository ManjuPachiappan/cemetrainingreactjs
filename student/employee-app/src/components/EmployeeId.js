import React from "react";

const EmployeeId = (props) => (
	<h6 className="card-subtitle mb-2 text-muted">{props.id}</h6>
);

export default EmployeeId;
