import React from "react";

const EmployeeAge = (props) => props.visible && <h6>{props.age}</h6>;

export default EmployeeAge;
