import React from "react";

const EmployeeSalary = (props) => props.visible && <h6>{props.salary}</h6>;

export default EmployeeSalary;
