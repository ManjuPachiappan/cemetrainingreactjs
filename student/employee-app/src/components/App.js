import React, { useState, useEffect } from "react";
import Employee from "./Employee";
import Banner from "./Banner";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.css";
import axios from "axios";
import CreateEmployeeForm from "./CreateEmployeeForm";

const App = () => {
	const [employeeList, setEmployeeList] = useState([]);
	const [fetchEmployee, setFetchEmployee] = useState(false);

	useEffect(() => {
		console.log("App is mounted");
		axios.get("http://localhost:8088/api/all").then((res) => {
			console.log(res);
			setEmployeeList(res.data);
		});
	}, [fetchEmployee]);
	/* const employeeList = [
		{
			id: 1,
			name: "Manju",
			age: 25,
			salary: 2000,
			email: "manju@allstate.com",
		},
		{
			id: 2,
			name: "Joe",
			age: 25,
			salary: 2000,
			email: "joe@allstate.com",
		},
	]; */

	return (
		<Router>
			<div className="app">
				<nav>
					<ul>
						<li>
							<Link to="/">Home</Link>
						</li>
						<li>
							<Link to="/add">Add Employee</Link>
						</li>
					</ul>
				</nav>
				<Banner />
				<Switch>
					<Route exact path="/">
						<div className="container">
							<div className="row">
								{employeeList.map((employee) => (
									<Employee
										key={employee.id}
										id={employee.id}
										name={employee.name}
										age={employee.age}
										email={employee.email}
										salary={employee.salary}
									/>
								))}
							</div>
						</div>
					</Route>
					<Route path="/add">
						{
							<CreateEmployeeForm
								fetchEmployee={fetchEmployee}
								setFetchEmployee={setFetchEmployee}
							/>
						}
					</Route>
				</Switch>
			</div>
		</Router>
	);
};

export default App;
