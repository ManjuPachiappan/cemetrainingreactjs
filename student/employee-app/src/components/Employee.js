import React, { useState } from "react";
import EmployeeId from "./EmployeeId";
import EmployeeName from "./EmployeeName";
import EmployeeAge from "./EmployeeAge";
import EmployeeEmail from "./EmployeeEmail";
import EmployeeSalary from "./EmployeeSalary";
import ShowHideButton from "./ShowHideButton";

const Employee = ({ id, name, age, email, salary }) => {
	const [visible, setVisibity] = useState(true);

	return (
		<div className="col-md-4">
			<div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
				<div className="card-body">
					<EmployeeId id={id} />
					<EmployeeName name={name} />
					<EmployeeAge age={age} visible={visible} />
					<EmployeeEmail email={email} visible={visible} />
					<EmployeeSalary salary={salary} visible={visible} />
					<ShowHideButton toggle={setVisibity} visible={visible} />
				</div>
			</div>
		</div>
	);
};

export default Employee;
