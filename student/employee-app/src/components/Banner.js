import React from "react";

const Banner = () => (
	<section className="jumbotron text-center">
		<div className="container">
			<h1 className="jumbotron-heading">Employee Details</h1>
			<p className="lead text-muted">
				Employee Database management system. This application handles the
				employee data with all the details.
			</p>
			<p>
				{/* <a
					href="https://bitbucket.org/neohenry/cemetrainingreactjs/src/master/instructor/album-app/"
					className="btn btn-secondary my-2"
					target="_blank"
					rel="noreferrer"
				>
					Get the source code
				</a> */}
			</p>
		</div>
	</section>
);

export default Banner;
