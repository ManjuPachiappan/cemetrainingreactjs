import Greet from "./Greet";
import React from "react";

class Person extends React.Component {
	constructor(props) {
		super(props);
		this.state = { height: 185, weight: 200 };
		this.growHeight = this.growHeight.bind(this);
		this.growWeight = this.growWeight.bind(this);
	}
	growHeight = () => {
		this.setState({ height: this.state.height + 1 });
	};
	growWeight = () => {
		this.setState({ weight: this.state.weight + 1 });
	};
	render() {
		console.log(this.state);
		return (
			<div>
				<Greet person={this.props.name} name="Allstate"></Greet>
				<p>
					{this.props.name} weighs {this.state.weight}lbs and is{" "}
					{this.state.height} cm tall.
				</p>
				<button onClick={this.growHeight}>GrowHeight</button>
				<button onClick={this.growWeight}>GrowWeight</button>
			</div>
		);
	}
}
export default Person;
