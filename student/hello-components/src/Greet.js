import React from "react";

/* class Greet extends React.Component {
	constructor(props) {
		super(props);
		console.log(props);
	}
	render() {
		return (
			<h1>
				Class Component- Welcome {this.props.name} from {this.props.person}
			</h1>
		);
	}
} */
/* function Greet({ name, person }) {
	//console.log(props);
	return (
		<h1>
			Hello {name} from {person}!!!
		</h1>
	);
}
 */
const Greet = ({ name, person }) => (
	<h5>
		Hello {person} from {name}
	</h5>
);

export default Greet;
