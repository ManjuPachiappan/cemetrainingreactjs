import React, { useState } from "react";
import ReactDOM from "react-dom";
import Person from "./Person";
import Greet from "./Greet";

const PersonFunc = ({ name }) => {
	const [height, setHeight] = useState(185);
	const [weight, setWeight] = useState(200);
	return (
		<div>
			<Greet person={name} name="Allstate"></Greet>
			<p>
				{name} weighs {weight}lbs and is {height} cm tall.
			</p>
			<button onClick={() => setHeight(height + 1)}>GrowHeight</button>
			<button onClick={() => setWeight(weight + 1)}>GrowWeight</button>
		</div>
	);
};

const main = (
	<div>
		<Person name="Manju" />
		<PersonFunc name="May" />
	</div>
);

ReactDOM.render(main, document.querySelector("#root"));
