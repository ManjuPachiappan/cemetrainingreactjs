import React, { useState, useEffect } from "react";
import "./App.css";
import Album from "./Album";
import axios from "axios";

const App = () => {
	const [albums, setAlbums] = useState([]);

	useEffect(() => {
		console.log("App is mounted");
		axios.get("http://localhost:8088/albums").then((res) => {
			console.log(res);
			setAlbums(res.data);
		});
	}, []);
	/* const albums = [
		{
			title: "Album 1",
			artist: "Artist 1",
			tracks: ["Track 1", "Track 2", "Track 3"],
		},
		{
			title: "Album 2",
			artist: "Artist 2",
			tracks: ["Track 4", "Track 5", "Track 6"],
		},
		{
			title: "Album 3",
			artist: "Artist 3",
			tracks: ["Track 7", "Track 8", "Track 9"],
		},
	]; */

	return (
		<div className="app">
			<div>
				{albums.map((album) => (
					<Album
						key={album.title}
						title={album.title}
						artist={album.artist}
						tracks={album.tracks}
					/>
				))}
			</div>
		</div>
	);
};
export default App;
