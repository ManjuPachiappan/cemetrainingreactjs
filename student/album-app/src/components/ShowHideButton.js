import React from "react";

const ShowHideButton = (props) => {
	return <button onClick={() => props.toggle(!props.visible)}>Toggle</button>;
};
export default ShowHideButton;
