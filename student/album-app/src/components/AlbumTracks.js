import React from "react";

const AlbumTracks = (props) => {
	return (
		props.visible && <p>{props.tracks} tracks</p>
		/* { <ol className="album-tracks">
				{props.track.map((tracks) => (
					<li key={tracks}>{tracks}</li>
				))}
			</ol> } */
	);
};
export default AlbumTracks;
