import React from "react";

const AlbumArtist = (props) => {
	return <h5 className="album-title">{props.artist}</h5>;
};
export default AlbumArtist;
