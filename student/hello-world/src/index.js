import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

/* const head1=React.createElement("p", {className:"hello-text",id:"hello-id"}, "Hello World!!!");
const main=React.createElement("div",{className:"hello-container"},head1); */
const shopName = "Allstate Ice cream shop";
function getFlavours() {
	return (
		<>
			<h2>Ice Cream Flavours</h2>
			<ul className="ice-cream-list">
				<li>Vanilla</li>
				<li>Chocolate</li>
				<li className="raspberry-color">Raspberry Ripple</li>
				<li>Cookies & Cream</li>
			</ul>
		</>
	);
}
const main = (
	<section>
		<div>
			<h1>{shopName}</h1>
		</div>
		<div id="ice-cream">{getFlavours()}</div>
	</section>
);

ReactDOM.render(main, document.getElementById("root"));
